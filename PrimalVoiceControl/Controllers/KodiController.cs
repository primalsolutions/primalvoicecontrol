﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace PrimalVoiceControl.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class KodiController : ControllerBase
    {
        private string[] approvedCommands = { "play", "pause", "stop", "lower", "volume", "increase" };
        private KodiWrapper KodiWrapper { get; set; }
        public KodiController(IConfiguration configuration) 
        {
            var kodiSettings = configuration.GetSection("KodiSettings").Get<KodiSettings>();

            KodiWrapper = new KodiWrapper(kodiSettings);
        }
        [HttpGet]
        [Route("Command")]
        public string GetCommand(string command)
        {
            string kodiCommand = string.Empty;

            switch (approvedCommands.FirstOrDefault(s => command.Contains(s)))
            {
                case "play":
                    kodiCommand = KodiWrapper.PlayPlayer();
                    break;
                case "pause":
                    kodiCommand = KodiWrapper.PausePlayer();
                    break;
                case "volume":
                case "lower":
                case "increase":
                    if (command.Contains("lower"))
                    {
                        int newVolume = Converter.ParseCommand.ParseIntFromCommand(command);
                        kodiCommand = KodiWrapper.SetVolume(-newVolume);
                    }
                    else if (command.Contains("increase"))
                    {
                        int newVolume = Converter.ParseCommand.ParseIntFromCommand(command);
                        kodiCommand = KodiWrapper.SetVolume(newVolume);
                    }
                    
                    break;
                case "stop":
                    kodiCommand = KodiWrapper.PlayerStopTest();
                    break;
                default:
                    break;
            }

            return kodiCommand;
        }
    }
}
