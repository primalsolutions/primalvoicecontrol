﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PrimalVoiceControl.Models;
using RestSharp;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace PrimalVoiceControl.Mappers
{
    public static class Mapper
    {
        private static JsonSerializerSettings jsonSerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            },
            Formatting = Formatting.Indented
        };
        private static string Serialize<T>(T serializeObj)
        {
            return JsonConvert.SerializeObject(serializeObj, jsonSerializerSettings);
        }
        public static RestRequest MapToRequest(string method, Dictionary<string, object> parameters)
        {
            KodiRequestPayload payload = new KodiRequestPayload()
            {
                Jsonrpc = "2.0",
                Method = method,
                Params = parameters,
                Id = 1
            };
            var serializedPayload = Serialize(payload);
            var request = new RestRequest(Method.POST);
            request.AddParameter($"application/json", serializedPayload, ParameterType.RequestBody);
            return request;
        }
    }
}
