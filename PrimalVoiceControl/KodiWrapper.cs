﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PrimalVoiceControl.Models;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using PrimalVoiceControl.Mappers;


namespace PrimalVoiceControl
{
    public class KodiWrapper
    {
        public RestClient Client { get; set; }
        public KodiWrapper(KodiSettings kodiSettings)
        {
            Client = new RestClient(kodiSettings.Url)
            {
                Timeout = -1
            };

            Client.Authenticator = new HttpBasicAuthenticator(kodiSettings.Username, kodiSettings.Password);
            Client.AddDefaultHeader("Content-Type", "application/json");
        }

        public int GetPlayerSpeed()
        {
            int playerParsedSpeed;
            var request = Mapper.MapToRequest("Player.GetProperties", new Dictionary<string, object>() { { "playerid", 1 },
                    {"properties", new string[] { "speed" } } });
            IRestResponse response = Client.Execute(request);
            KodiResponsePayload kodiResponse = JsonConvert.DeserializeObject<KodiResponsePayload>(response.Content);
            kodiResponse.Result.TryGetValue("speed", out object playerSpeed);
            if (int.TryParse(playerSpeed.ToString(), out playerParsedSpeed))
            {
                return playerParsedSpeed;
            }
            throw new FormatException();
        }
        public string PausePlayer()
        {
            if (GetPlayerSpeed() != 0)
            {
                var request = Mapper.MapToRequest("Player.PlayPause", new Dictionary<string, object>() { { "playerid", 1 } });
                IRestResponse response = Client.Execute(request);
                return response.Content;
            }
            return string.Empty;
        }
        public string PlayPlayer()
        {
            if (GetPlayerSpeed() == 0)
            {
                var request = Mapper.MapToRequest("Player.PlayPause", new Dictionary<string, object>() { { "playerid", 1 } });
                IRestResponse response = Client.Execute(request);
                return response.Content;
            }
            return string.Empty;
        }

        public string PlayerStopTest()
        {
            var request = Mapper.MapToRequest("Player.Stop", new Dictionary<string, object>() { { "playerid", 1 } });
            IRestResponse response = Client.Execute(request);
            return response.Content;
        }
        public long GetVolume()
        {
            long playerVolume;
            var request = Mapper.MapToRequest("Application.GetProperties", new Dictionary<string, object>() { { "properties", new string[] { "volume" } } });
            IRestResponse response = Client.Execute(request);
            KodiResponsePayload kodiResponse = JsonConvert.DeserializeObject<KodiResponsePayload>(response.Content);
            kodiResponse.Result.TryGetValue("volume", out object volume);
            if (long.TryParse(volume.ToString(), out playerVolume))
            {
                return playerVolume;
            }
            throw new FormatException();

        }
        public string SetVolume(int offset)
        {
            var currentVol = GetVolume();
            var request = Mapper.MapToRequest("Application.SetVolume", new Dictionary<string, object>() { { "volume", currentVol + offset } });
            IRestResponse response = Client.Execute(request);
            return response.Content;
        }
    }
}
