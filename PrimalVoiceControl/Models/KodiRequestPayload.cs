﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrimalVoiceControl.Models
{
    public class KodiRequestPayload
    {
        public string Jsonrpc { get; set; }
        public string Method { get; set; }
        public Dictionary<string, object> Params { get; set; }
        public int Id { get; set; }
    }
}
