﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrimalVoiceControl.Models
{
    public class KodiResponsePayload
    {
        public int Id { get; set; }
        public string Jsonrpc { get; set; }
        public Dictionary<string, object> Result { get; set; }
    }

}
