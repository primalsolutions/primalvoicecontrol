﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrimalVoiceControl.Converter
{
    public class ParseCommand
    {
        public static int ParseIntFromCommand(string command)
        {
            string[] commandList = command.Split(" ");
            int commandInt = 0;

            foreach (var item in commandList)
            {
                if (int.TryParse(item, out commandInt))
                {
                    break;
                }
            }
            return commandInt;
        }
    }
}
