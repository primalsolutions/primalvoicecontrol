﻿namespace PrimalVoiceControl
{
    public class KodiSettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
    }
}