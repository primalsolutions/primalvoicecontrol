﻿using System.Collections.Generic;

namespace PrimalVoiceControl
{
    public class DynamicConnectionSettings
    {
        public string[] Connections { get; set; }
    }
}